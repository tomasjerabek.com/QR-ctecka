**QR čtečka**

Jednoduchá QR čtečka pro skenování do seznamu s časovým razítkem. Výsledek je možné sdílet jako text; každá hodnota je oddělena novým řádkem, separátorem je tabulátor.

Příklad:

<img src="/uploads/9c5086f003f8c2d81e18adf95320bc9d/Screenshot__12._6._2018_20_11_59_.png"  width="200">


test1	12.06.18 20:10:12

pokusný text 2	12.06.18 20:10:36


Import do tabulkového procesoru:

<img src="/uploads/7dfc8eba909493e1133071905316b027/screenshot_2018-06-12_v_20.12.47.png"  width="300">


<img src="/uploads/19f6dacc6f95ac1cedb93a6431b23f53/screenshot_2018-06-12_v_20.12.23.png"  width="200">
