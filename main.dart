import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:qrcode_reader/QRCodeReader.dart';
import 'package:share/share.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'QR čtečka',
      home: new MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;
  final Map<String, dynamic> pluginParameters = {};

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Future<String> _barcodeString;
  Map _barcodeValues = new Map();
  int index = 0;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: const Text('QR čtečka'),
      ),
      body: new Center(
          child: new FutureBuilder<String>(
              future: _barcodeString,
              builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                if (snapshot.data == null) {
                  return new Container();
                } else {
                  var timestamp = new DateTime.now();
                  var formatter = new DateFormat('dd.MM.yy HH:mm:ss');
                  String formatted = formatter.format(timestamp);

                  if (_barcodeValues.containsKey(snapshot.data)) {
                  } else {
                    _barcodeValues[snapshot.data] = formatted;
                  }
                  var keys = _barcodeValues.keys.toList();

                  return new ListView.builder(
                      itemBuilder: (context, index) => new ListTile(
                            leading: new Text('$index'),
                            title: new Text(keys[index]),
                            trailing: new Text(_barcodeValues[keys[index]]),
                          ),
                      itemCount: _barcodeValues.length,
                      reverse: true);
                }
              })),
      bottomNavigationBar: new BottomNavigationBar(
        currentIndex: index,
        onTap: (int index) {
          if (index == 1) {
            setState(() {
              _barcodeString = new QRCodeReader()
                  .setAutoFocusIntervalInMs(200)
                  .setForceAutoFocus(true)
                  .setTorchEnabled(true)
                  .setHandlePermissions(true)
                  .setExecuteAfterPermissionGranted(true)
                  .scan();
            });
          }

          if (index == 0) {
            String csv = "";
            _barcodeValues.forEach((k, v) => csv = csv + "$k\t$v\r\n");
            share('$csv');
          }

          setState(() {
            this.index = index;
          });
        },
        items: <BottomNavigationBarItem>[
          new BottomNavigationBarItem(
            icon: new Icon(Icons.share),
            title: new Text("Sdílet"),
          ),
          new BottomNavigationBarItem(
            icon: new Icon(
              Icons.add_a_photo,
            ),
            title: new Text("Skenuj QR kód"),
          ),
        ],
      ),
    );
  }
}
